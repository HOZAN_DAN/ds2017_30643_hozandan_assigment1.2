package com.javawebtutor.controller;

import com.javawebtutor.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        UserService userService = new UserService();
        userService.deleteUserById(userId);
            response.sendRedirect("homeAdmin.jsp");
    }


}
