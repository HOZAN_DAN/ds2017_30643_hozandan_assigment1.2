package com.javawebtutor.controller;

import com.javawebtutor.service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteFlightServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FlightService flightService = new FlightService();

        String idFlight = request.getParameter("id");
        flightService.deleteFlightById(Integer.parseInt(idFlight));
        response.sendRedirect("homeAdmin.jsp");
    }

}
