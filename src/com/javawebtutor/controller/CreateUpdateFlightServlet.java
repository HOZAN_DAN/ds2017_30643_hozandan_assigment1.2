package com.javawebtutor.controller;

import com.javawebtutor.model.Flight;
import com.javawebtutor.service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class CreateUpdateFlightServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = new Flight();
        FlightService flightService = new FlightService();


        DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dfHour = new SimpleDateFormat("HH:mm:ss");

        if (!request.getParameter("id").equals("new"))
            flight.setId(Integer.parseInt(request.getParameter("id")));

        flight.setAirplane_type(request.getParameter("airplane_type"));

        flight.setFlight_number(Integer.parseInt(request.getParameter("flight_number")));

        flight.setArrival_city(request.getParameter("arrival_city"));
        flight.setDeparture_city(request.getParameter("departure_city"));


        try {
            flight.setArrival_date(dfDate.parse(request.getParameter("arrival_date")));
            flight.setArrival_houer(dfHour.parse(request.getParameter("arrival_hour")));
            flight.setDeparture_date(dfDate.parse(request.getParameter("departure_date")));
            flight.setDeparture_houer(dfHour.parse(request.getParameter("departure_hour")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        flightService.saveOrUpdateFlight(flight);
        response.sendRedirect("homeAdmin.jsp");
    }

}
