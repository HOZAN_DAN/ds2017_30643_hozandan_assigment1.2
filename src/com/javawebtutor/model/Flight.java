package com.javawebtutor.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="FLIGHT")
public class Flight implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private int flight_number;
    private String airplane_type;

    private String departure_city;
    @Temporal(TemporalType.DATE)
    private Date departure_date;
    @Temporal(TemporalType.TIME)
    private Date departure_houer;

    private String arrival_city;
    @Temporal(TemporalType.DATE)
    private Date arrival_date;
    @Temporal(TemporalType.TIME)
    private  Date arrival_houer;

    public Flight() {
    }

    public Flight(int flight_number, String airplane_type, String departure_city,
                  Date departure_date, Date departure_houer,
                  String arrival_city, Date arrival_date, Date arrival_houer) {
        this.flight_number = flight_number;
        this.airplane_type = airplane_type;
        this.departure_city = departure_city;
        this.departure_date = departure_date;
        this.departure_houer = departure_houer;
        this.arrival_city = arrival_city;
        this.arrival_date = arrival_date;
        this.arrival_houer = arrival_houer;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(int flight_number) {
        this.flight_number = flight_number;
    }

    public String getAirplane_type() {
        return airplane_type;
    }

    public void setAirplane_type(String airplane_type) {
        this.airplane_type = airplane_type;
    }

    public String getDeparture_city() {
        return departure_city;
    }

    public void setDeparture_city(String departure_city) {
        this.departure_city = departure_city;
    }


    public String getArrival_city() {
        return arrival_city;
    }

    public void setArrival_city(String arrival_city) {
        this.arrival_city = arrival_city;
    }

    public Date getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(Date departure_date) {
        this.departure_date = departure_date;
    }

    public Date getDeparture_houer() {
        return departure_houer;
    }

    public void setDeparture_houer(Date departure_houer) {
        this.departure_houer = departure_houer;
    }

    public Date getArrival_date() {
        return arrival_date;
    }

    public void setArrival_date(Date arrival_date) {
        this.arrival_date = arrival_date;
    }

    public Date getArrival_houer() {
        return arrival_houer;
    }

    public void setArrival_houer(Date arrival_houer) {
        this.arrival_houer = arrival_houer;
    }
}
