package com.javawebtutor.service;

import com.javawebtutor.hibernate.util.HibernateUtil;
import com.javawebtutor.model.Flight;
import com.javawebtutor.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class FlightService {


    public List<Flight> getFlightsByArriveCity(String arrive_city) {
        List<Flight> flights = new ArrayList<>();
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            flights = session.createQuery("from Flight where arrival_city='" + arrive_city + "'").list();

            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights;
    }

    public void deleteFlightById(int flightId) {
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("delete from Flight where id=" + flightId + "");
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }

    }

    public List<Flight> getFlights() {
        List<Flight> flights = new ArrayList<>();
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            flights = session.createQuery("from Flight").list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights;
    }


    public boolean saveOrUpdateFlight(Flight flight) {
        Session session = HibernateUtil.openSession();

        Transaction tx = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            session.saveOrUpdate(flight);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.getMessage();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

}
