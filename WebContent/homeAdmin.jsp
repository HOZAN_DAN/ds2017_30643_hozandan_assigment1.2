<%@page import="java.util.List" %>
<%@page import="com.javawebtutor.service.UserService" %>
<%@page import="com.javawebtutor.model.User" %>
<%@ page import="com.javawebtutor.service.FlightService" %>
<%@ page import="com.javawebtutor.model.Flight" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Admin Page</title>
    <style>
        .customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .customers td, .customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .customers tr:hover {
            background-color: #ddd;
        }

        .customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>

<body>

<br/>
<a href="logout.jsp">Logout</a>


<form action="CreateUpdateFlightServlet" method="POST">
    <table cellpadding="5">
        <tr>
            <td>Id </td>
            <td><input type="text" name="id" value="new" maxlength="30"/></td>
        </tr>
        <tr>
            <td>Airplane type</td>
            <td><input type="text" name="airplane_type" maxlength="30"/></td>
        </tr>
        <tr>
            <td>Arrival city</td>
            <td><input type="text" name="arrival_city" maxlength="100"/></td>
        </tr>
        <tr>
            <td>Arrival date</td>
            <td><input type="text" name="arrival_date" maxlength="100"/></td>
        </tr>
        <tr>
        <tr>
            <td>Arrival hour</td>
            <td><input type="text" name="arrival_hour" maxlength="100"/></td>
        </tr>
        <tr>
            <td>Departure city</td>
            <td><input type="text" name="departure_city" maxlength="100"/></td>
        </tr>
        <tr>
            <td>Departure date</td>
            <td><input type="text" name="departure_date" maxlength="100"/></td>
        </tr>
        <tr>
            <td>Departure hour</td>
            <td><input type="text" name="departure_hour" maxlength="100"/></td>
        </tr>
        <tr>
            <td>Flight number</td>
            <td><input type="text" name="flight_number" maxlength="100"/></td>
        </tr>

        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="Submit">
                <input type="reset" value="Reset">
            </td>
        </tr>
    </table>
</form>

<div style="padding: 3% 0% 1% 0%">
    <form action="DeleteFlightServlet" method="post">
        
        <input name="id" size="10" />
        <input type="submit" value="Delete flight"/>
    </form>
</div>


<table class="customers">

    <thead>
    <tr>
        <th>ID</th>
        <th>Flight number</th>
        <th>Airplane type</th>
        <th>Departure city</th>
        <th>Departure date</th>
        <th>Departure hour</th>
        <th>Arrival city</th>
        <th>Arrival date</th>
        <th>Arrival hour</th>
    </tr>
    </thead>

    <tbody>




        <%
        String role = null;

        if( session.getAttribute( "principal" ) != null){
           role = ((User) session.getAttribute( "principal" )).getRole();
                if(!role.equals("admin"))
		            response.sendRedirect("login.jsp");
         }else
		   response.sendRedirect("login.jsp");


		FlightService flightService = new FlightService();
		List<Flight> flights = flightService.getFlights();
		for (Flight f: flights) {
		%>





    <tr>
        <td><%=f.getId()%>
        </td>
        <td><%=f.getFlight_number()%>
        </td>
        <td><%=f.getAirplane_type()%>
        </td>
        <td><%=f.getDeparture_city()%>
        </td>
        <td><%=f.getDeparture_date()%>
        </td>
        <td><%=f.getDeparture_houer()%>
        </td>
        <td><%=f.getArrival_city()%>
        </td>
        <td><%=f.getArrival_date()%>
        </td>
        <td><%=f.getArrival_houer()%>
        </td>
    </tr>
        <%}%>
    <tbody>
</table>


<div style="padding: 3% 0% 1% 0%">
    <form action="DeleteUserServlet" method="post">
        <input name="userId" size="10"/>
        <input type="submit" value="Delete User"/>
    </form>
</div>


<table class="customers">

    <thead>
    <tr>
        <th>User ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>email</th>
    </tr>
    </thead>
    <tbody>
        <%
			 UserService userService = new UserService();
			 List<User> list = userService.getListOfUsers();
			    for (User u : list) {
			%>
    <tr>
        <td><%=u.getUserId()%>
        </td>
        <td><%=u.getFirstName()%>
        </td>
        <td><%=u.getLastName()%>
        </td>
        <td><%=u.getEmail()%>
        </td>


    </tr>
        <%}%>
    <tbody>
</table>


</body>
</html>
