<%@page import="java.util.List" %>
<%@ page import="com.javawebtutor.service.FlightService" %>
<%@ page import="com.javawebtutor.model.Flight" %>
<%@ page import="com.javawebtutor.service.CityService" %>
<%@ page import="com.javawebtutor.model.User" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Result Page</title>
    <style>
        .customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .customers tr:hover {
            background-color: #ddd;
        }

        .customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>





<a href="logout.jsp">Logout</a>
</p>

<div style="padding: 3% 0% 1% 0%">

    <input name="id" size="10" />
    <input type="submit" value="search"/>
    </form>
</div>

<table class="customers">
    <thead>
    <tr>
        <th>Flight number</th>
        <th>Airplane type</th>
        <th>Departure city</th>
        <th>Departure date</th>
        <th>Departure hour</th>
        <th>Arrival city</th>
        <th>Arrival date</th>
        <th>Arrival hour</th>
    </tr>
    </thead>
    <tbody>
        <%
        String role = null;

        if( session.getAttribute( "principal" ) != null){
           role = ((User) session.getAttribute( "principal" )).getRole();
                if(!role.equals("user"))
		            response.sendRedirect("login.jsp");
         }else
		   response.sendRedirect("login.jsp");

		FlightService flightService = new FlightService();
		List<Flight> flights = flightService.getFlights();
		CityService cityService = new CityService();


		for (Flight f: flights) {
		%>
    <tr>
        <td><%=f.getFlight_number()%>
        </td>
        <td><%=f.getAirplane_type()%>
        </td>
        <td><%= cityService.getCityById(Integer.parseInt(f.getDeparture_city())).getName()%>
        </td>
        <td><%=f.getDeparture_date()%>
        </td>
        <td><%=f.getDeparture_houer()%>
        </td>
        <td><%=cityService.getCityById(Integer.parseInt(f.getArrival_city())).getName()%>
        </td>
        <td><%=f.getArrival_date()%>
        </td>
        <td><%=f.getArrival_houer()%>
        </td>
    </tr>
        <%}%>
    <tbody>
</table>


</body>
</html>
